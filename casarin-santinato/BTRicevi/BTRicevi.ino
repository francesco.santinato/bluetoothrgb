/* Riceve messaggi che vengono inviati via Bluetooth HC-05 da una app.
*/

#include <SoftwareSerial.h>

const int RXPin = 10;  // da collegare su TX di HC05
const int TXPin = 11; // da collegare su RX di HC05
const int  redpin = 3;
const int  greenpin = 4;
const int  bluepin = 5;


//creo una nuova porta seriale via software
SoftwareSerial myBT = SoftwareSerial(RXPin, TXPin);

char msgChar;

void setup()
{
  pinMode(RXPin, INPUT);
  pinMode(TXPin, OUTPUT);
  pinMode(greenpin, OUTPUT);
  pinMode(bluepin, OUTPUT);
  pinMode(redpin, OUTPUT);
  myBT.begin(38400);

  Serial.begin(9600);
}

String messaggio = "", r, g, b;
void loop()

{
  if (messaggio != "") {
    //Elabora la stringa
    Serial.print(messaggio);
    r = messaggio[0]; r += messaggio[1]; r += messaggio[2];
    g = messaggio[4]; g += messaggio[5]; g += messaggio[6];
    b = messaggio[8]; b += messaggio[9]; b += messaggio[10];
    rgb(r.toInt(), g.toInt(), b.toInt());
    messaggio = ""; r = ""; b = ""; g = "";
  }
  while (myBT.available()) {
    msgChar = myBT.read();
    messaggio += char(msgChar);
  }
}

void rgb(int rosso, int verde, int blu) {
  analogWrite(redpin, rosso);
  analogWrite(greenpin, verde);
  analogWrite(bluepin, blu);
}
