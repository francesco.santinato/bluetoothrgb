package com.example.bluetooth1;

import android.bluetooth.BluetoothSocket;

public class SocketSD {

    public static BluetoothSocket socket;

    public static void setSocket(BluetoothSocket socket) {
        SocketSD.socket = socket;
    }

    public static BluetoothSocket getSocket() {
        return socket;
    }
}
