package com.example.bluetooth1;

import androidx.appcompat.app.AppCompatActivity;

import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStreamWriter;

public class SubActivity extends AppCompatActivity {
    LinearLayout linearLayoutCerchio;
    SeekBar seekBarTrasparenzaImmagine;
    SeekBar seekBarRosso;
    SeekBar seekBarVerde;
    SeekBar seekBarBlu;
    TextView textViewRosso;
    TextView textViewVerde;
    TextView textViewBlu;
    TextView textViewTrasparenza;
    OutputStreamWriter writer;
    BluetoothSocket verySocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub);

        linearLayoutCerchio = (LinearLayout) findViewById(R.id.linearLayoutCerchio);
        seekBarTrasparenzaImmagine = (SeekBar) findViewById(R.id.seekBarTrasparenzaImmagine);
        seekBarRosso = (SeekBar) findViewById(R.id.seekBarRosso);
        seekBarVerde = (SeekBar) findViewById(R.id.seekBarVerde);
        seekBarBlu = (SeekBar) findViewById(R.id.seekBarBlu);
        textViewRosso = (TextView) findViewById(R.id.textViewRosso);
        textViewVerde = (TextView) findViewById(R.id.textViewVerde);
        textViewBlu = (TextView) findViewById(R.id.textViewBlu);
        textViewTrasparenza = (TextView) findViewById(R.id.textViewTrasparenza);

        seekBarRosso.setOnSeekBarChangeListener(seekBarChangeListenerColore);
        seekBarVerde.setOnSeekBarChangeListener(seekBarChangeListenerColore);
        seekBarBlu.setOnSeekBarChangeListener(seekBarChangeListenerColore);
        seekBarTrasparenzaImmagine.setOnSeekBarChangeListener(seekBarChangeListener);

        verySocket = SocketSD.getSocket();
    }

    private SeekBar.OnSeekBarChangeListener seekBarChangeListener
            = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            GradientDrawable myCircle = (GradientDrawable) linearLayoutCerchio.getBackground();
            myCircle.setAlpha(progress);
            textViewTrasparenza.setText("" + progress );
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    private SeekBar.OnSeekBarChangeListener seekBarChangeListenerColore
            = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            GradientDrawable myCircle = (GradientDrawable) linearLayoutCerchio.getBackground();
            if (seekBar == seekBarRosso){
                textViewRosso.setText("" + progress);
            }
            if (seekBar == seekBarVerde){
                textViewVerde.setText("" + progress);
            }
            if (seekBar == seekBarBlu){
                textViewBlu.setText("" + progress);
            }
            int progress_rosso = seekBarRosso.getProgress();
            int progress_verde = seekBarVerde.getProgress();
            int progress_blu = seekBarBlu.getProgress();
            myCircle.setColor(
                    0xFF000000
                            + progress_rosso * 0x10000
                            + progress_verde * 0x100
                            + progress_blu
            );
            /*String red = Integer.toString(progress_rosso);
            String green = Integer.toString(progress_verde);
            String blu = Integer.toString(progress_blu);

            String color = red + "!" + green + "!" + blu + "!";*/
            String color = String.format("%03d!%03d!%03d!",progress_rosso,progress_verde,progress_blu);
            try {
                writer = new OutputStreamWriter(verySocket.getOutputStream());
                writer.write(color);
                writer.flush();
            } catch (IOException e){

            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };
}

